let data = require('./1-users.cjs')

function groupUser(data) {
    let obj = {
        "Javascript": [],
        "Python": [],
        "Golang": []
    }
    let result = Object.entries(data).reduce((prev, curr) => {
        let language = curr[1].desgination.split(" ");
        let value = ["Javascript", "Golang", "Python"].find((item) => language.includes(item));
        prev[value].push(curr[1]);
        return prev;

    }, obj)
    return result;
}
groupUser(data);
console.log(groupUser(data));